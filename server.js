const nconf = require('./config');
const UDPserver = require('./UDP/server');
const UDPclient = require('./UDP/client');
const TCPclient = require('./TCP/client');
const TCPserver = require('./TCP/server');

function initServers() {
  const TCP = TCPserver.InitTCP(nconf.get('TCP:port'), nconf.get('TCP:host'));
  const UDP = UDPserver.InitUDP(nconf.get('UDP'));
}

initServers();

for (let i = 0; i < 10; i++) {
  TCPclient.sendMessage(new Date().toISOString());
}

for (let i = 0; i < 10; i++) {
  UDPclient.sendMessage(hostUDP, portUDP, new Date().toISOString());
}


module.exports = initServers;