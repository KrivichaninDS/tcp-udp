const dgram = require('dgram');


const InitUDP = (port, host) => {
  return new UDPserver(port, host);
};

class UDPserver {
  constructor(port, host) {
    this.port = port;
    this.host = host;
    this.ping = 0;
    this.serverUDP = dgram.createSocket('udp4');

    this.serverUDP.on('error', (err) => {
      console.log(`UDP server error:\n${err.stack}`);
      this.serverUDP.close();
    });

    this.serverUDP.on('message', (msg, rinfo) => {
      this.ping = Date.now() - Date.parse(msg);
      console.log(`UDP server got: message: ${msg}. Ping: ${this.ping} ms`);
    });

    this.serverUDP.on('listening', () => {
      let address = this.serverUDP.address();
      console.log(`UDP server listening ${address.address}:${address.port}`
      );
    });

    this.serverUDP.bind(port, host);
  }
}

module.exports = {
  InitUDP: InitUDP
};
