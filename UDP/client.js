const dgram = require('dgram');

function sendMessage(host, port, message) {

  const client = dgram.createSocket('udp4');

  client.send(message, 0, message.length, port, host, (err, bytes) => {
    if (err) throw err;
    console.log(`UDP client sent to ${host} : ${port}. Bytes count: ${bytes}`);
    client.close();
  });
};

module.exports = {
  sendMessage: sendMessage
};
