const net = require('net');
const nconf = require('nconf');

class TCPclient {
  constructor() {
    this.client = net.connect(nconf.get('TCP'), () => {
      console.log(`TCP client connected to server`);
    });

    this.client.on('data', (data) => {
      console.log(`Server request: ${data}`);
    });

    this.client.on('close', () => {
      console.log(`TCP connection closed`)
    });
  }

  sendMessage(message) {
    this.client.write(message);
  }
}

module.exports = new TCPclient();