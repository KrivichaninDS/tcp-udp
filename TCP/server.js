const net = require('net');

const InitTCP = (port, host) => {
  return new TCPserver(port, host);
};

class TCPserver {
  constructor(port, host) {
    this.port = port;
    this.host = host;
    this.ping = 0;
    this.serverTCP = net.createServer((connection) => {

      connection.on('error', (err) => {
        console.log(`TCP server error:\n${err.stack}`);
        this.serverTCP.close();
      });

      connection.on('data', (data) => {
        let ping = Date.now() - Date.parse(data);
        console.log(`TCP server got message: ${data}. Ping: ${ping} ms\n`);
      });

      connection.on('close', () => {
        console.log(`TCP server closed`)
      });
    });

    this.serverTCP.on('connection', (socket) => {
      console.log(`New TCP client connected: ${socket.remoteAddress}: ${socket.remotePort}`);
    });

    this.serverTCP.listen(port, host, () => {
      console.log(`TCP server listening ${host}:${port}`);
    });
  }
}

module.exports ={
  InitTCP: InitTCP
};
